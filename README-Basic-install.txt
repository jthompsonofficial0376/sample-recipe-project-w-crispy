After the repo is completely there , 
in the terminal , in the root working directory


1.
- Activate the 
		virtual environment: Use source venv/bin/activate on Mac/Linux
 			or 
venv/Scripts/Activate.ps1   (  usually it is ./.venv/Scripts/Activate.ps1 , 
                                but for this project , we can ommit the ./. 


2.
Install dependencies:

pip install -r requirements.txt.



3. Optional( do it for this app ) - Create a admin superuser with a login
password  , for the 127.0.0.1/admin endpoint to login.  

- to do this , type  

python manage.py createsuperuser

This will prompt you too create a superuser. 


--------------------------------------------------
- if you need too , check migrations - 

4. python manage.py makemigrations 
 python manage.py migrate     




5. Then run the server , 

python manage.py runserver 


- You should now be able to visit the site in 
localhost ,  127.0.0.1/admin and 127.0.0.1 as a base. 